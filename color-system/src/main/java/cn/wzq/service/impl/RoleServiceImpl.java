package cn.wzq.service.impl;

import cn.wzq.constants.SystemCommonConstant;
import cn.wzq.domain.entity.Role;
import cn.wzq.mapper.RoleMapper;
import cn.wzq.service.IRoleService;
import cn.wzq.utils.SnowIdUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wzq
 * @Date 2021-03-09 17:48
 * @Version 1.0
 **/
@Service
public class RoleServiceImpl implements IRoleService {

    @Resource
    private RoleMapper roleMapper;

    @Override
    public int addRole(Role role) {
        role.setRoleId(SnowIdUtil.getSnowUid());
        role.setRoleStatus(SystemCommonConstant.COMMON);
        int count = roleMapper.qeuryRoleCount();
        role.setRoleOder(count+1);
        return roleMapper.addRole(role);
    }

    @Override
    public int deleteRole(String roleId) {
        Role role = new Role();
        role.setRoleId(roleId);
        role.setRoleStatus(SystemCommonConstant.DELETE);
        return  roleMapper.updateRole(role);
    }

    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    @Override
    public List<Role> queryRole() {
        return roleMapper.queryRoleList();
    }
}
