package cn.wzq.service.impl;

import cn.wzq.domain.entity.User;
import cn.wzq.mapper.UserMapper;
import cn.wzq.service.IUserService;
import cn.wzq.utils.SnowIdUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author wzq
 * @Date 2021-03-09 11:27
 * @Version 1.0
 **/
@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public int addUser(User user) {
        user.setUserId(SnowIdUtil.getSnowUid());
        return userMapper.addUser(user);
    }
}
