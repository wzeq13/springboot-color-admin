package cn.wzq.service;

import cn.wzq.domain.entity.User;

/**
 * @Author wzq
 * @Date 2021-03-09 11:26
 * @Version 1.0
 **/
public interface IUserService {
    /**
     * 添加用户
     *
     * @param user 用户信息
     * @return 结果
     */
    public int addUser(User user);
}
