package cn.wzq.service;

import cn.wzq.domain.entity.Role;
import java.util.List;

/**
 * @Author wzq
 * @Date 2021-03-09 17:42
 * @Version 1.0
 **/
public interface IRoleService {
    /**
     * 添加角色
     *
     * @param role 角色信息
     * @return 信息
     */
    public int addRole(Role role);

    /**
     * 删除角色
     *
     * @param roleId 角色id
     * @return 结果
     */
    public int deleteRole(String roleId);

    /**
     * 更新角色
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(Role role);

    /**
     * 查询角色
     *
     * @return 角色信息
     */
    public List<Role> queryRole();
}
