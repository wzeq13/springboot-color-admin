package cn.wzq.mapper;

import cn.wzq.domain.entity.Menu;
import java.util.List;

/**
 * @Author wzq
 * @Date 2021-03-15 13:43
 * @Version 1.0
 **/
public interface MenuMapper {
    /**
     * 添加菜单
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public int addMenu(Menu menu);

    /**
     * 删除菜单
     *
     * @param id 菜单id
     * @return 结果
     */
    public int deleteMenu(int id);

    /**
     * 更新菜单
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public int updateMenu(Menu menu);

    /**
     * 查询所有菜单
     *
     * @return 菜单列表
     */
    public List<Menu> queryAllMenu();
}