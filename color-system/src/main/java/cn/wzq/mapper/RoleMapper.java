package cn.wzq.mapper;

import cn.wzq.domain.entity.Role;
import java.util.List;

/**
 * @Author wzq
 * @Date 2021-03-09 17:00
 * @Version 1.0
 **/
public interface RoleMapper {
    /**
     * 添加角色
     *
     * @param role 角色信息
     * @return 结果
     */
    public int addRole(Role role);

    /**
     * 删除角色
     *
     * @param roleId 角色id
     * @return 结果
     */
    public int deleteRole(String roleId);

    /**
     * 更新角色
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(Role role);

    /**
     * 查询角色列表
     *
     * @return 结果
     */
    public List<Role> queryRoleList();

    /**
     * 查询角色数量
     *
     * @return 结果
     */
    public int qeuryRoleCount();
}
