package cn.wzq.domain.entity;

import cn.wzq.domain.BaseEntity;

/**
 * @Author wzq
 * @Date 2021-03-15 13:39
 * @Version 1.0
 **/
public class Menu extends BaseEntity {
    private String menuId;

    private String menuName;

    private Integer menuType;

    private String permission;

    private Integer menuOder;

    private String menuIcon;

    private String menuUrl;

    private Integer menuSatus;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getMenuOder() {
        return menuOder;
    }

    public void setMenuOder(Integer menuOder) {
        this.menuOder = menuOder;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getMenuSatus() {
        return menuSatus;
    }

    public void setMenuSatus(Integer menuSatus) {
        this.menuSatus = menuSatus;
    }
}
