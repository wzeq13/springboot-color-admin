package cn.wzq.domain.entity;

import cn.wzq.domain.BaseEntity;

/**
 * @Author wzq
 * @Date 2021-03-09 16:56
 * @Version 1.0
 **/
public class Role extends BaseEntity {
    /** 角色id **/
    private String roleId;

    /** 角色名 **/
    private String roleName;

    /** 角色状态 **/
    private Integer roleStatus;

    /** 排序 **/
    private Integer roleOder;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(Integer roleStatus) {
        this.roleStatus = roleStatus;
    }

    public Integer getRoleOder() {
        return roleOder;
    }

    public void setRoleOder(Integer roleOder) {
        this.roleOder = roleOder;
    }
}
