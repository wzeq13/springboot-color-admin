package cn.wzq.web.service;

import cn.wzq.utils.CaptchaUtil;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;

/**
 * @Author wzq
 * @Date 2021-03-15 15:23
 * @Version 1.0
 **/
@Service
public class CaptchaService {

    public void generateCaptcha(HttpServletRequest request, HttpServletResponse response){
        String captcha = "";
        try {
            captcha = CaptchaUtil.generate(request,response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //todo: 将验证码放入redis
    }
}
