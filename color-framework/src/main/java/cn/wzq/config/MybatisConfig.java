package cn.wzq.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wzq
 * @Date 2021-03-09 1:21
 * @Version 1.0
 **/
@Configuration
@MapperScan("cn.wzq.mapper")
public class MybatisConfig {
}
