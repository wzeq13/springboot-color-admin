package cn.wzq.domain;

/**
 * @Author wzq
 * @Date 2021-03-09 11:42
 * @Version 1.0
 **/
public class BaseController {
    public AjaxResult toAjax(int result){
        return result>0 ? AjaxResult.success() : AjaxResult.error();
    }
}
