package cn.wzq.domain;

import cn.wzq.enums.HttpCode;

/**
 * AjaxResult结果类
 *
 * @Author wzq
 * @Date 2021-03-07 23:25
 * @Version 1.0
 **/
public class AjaxResult {
    /** 返回码 **/
    private int code;

    /** 响应消息 **/
    private String msg;

    /** 返回数据 **/
    private Object data;

    /**
     * 构造函数
     *
     * @param code 状态码
     * @param msg 响应消息
     * @param data 响应数据
     */
    public AjaxResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 响应成功
     *
     * @return 结果
     */
    public static AjaxResult success(){
        return new AjaxResult(HttpCode.SUCCESS.getCode(),HttpCode.SUCCESS.getMsg(),null);
    }

    /**
     * 响应成功
     *
     * @param data 数据
     * @return 结果
     */
    public static AjaxResult success(Object data){
        AjaxResult ajaxResult = success();
        ajaxResult.setData(data);
        return ajaxResult;
    }

    /**
     * 响应成功
     *
     * @param msg 响应消息
     * @return 结果
     */
    public static AjaxResult success(String msg){
        AjaxResult ajaxResult = success();
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    /**
     * 响应失败
     *
     * @param code 状态码
     * @param msg 响应消息
     * @return 结果
     */
    public static AjaxResult error(int code,String msg){
        return new AjaxResult(code,msg,null);
    }

    /**
     * 响应失败
     *
     * @return 结果
     */
    public static AjaxResult error(){
        return error(HttpCode.FAILED.getCode(), HttpCode.FAILED.getMsg());
    }

    /**
     * 相应失败
     *
     * @param msg 响应消息
     * @return 结果
     */
    public static AjaxResult error(String msg){
        AjaxResult ajaxResult = error();
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
