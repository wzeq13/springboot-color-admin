package cn.wzq.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

/**
 * @Author wzq
 * @Date 2021-03-09 16:52
 * @Version 1.0
 **/
public class SnowIdUtil {
    public static String getSnowUid(){
        Snowflake snowflake = IdUtil.getSnowflake(1,1);
        return snowflake.nextIdStr();
    }
}
