package cn.wzq.constants;

/**
 * @Author wzq
 * @Date 2021-03-09 17:51
 * @Version 1.0
 **/
public interface SystemCommonConstant {

    /** 正常的 **/
    public static final Integer COMMON = 0;

    /** 停用的 **/
    public static final Integer STOP = 1;

    /** 删除的 **/
    public static final Integer DELETE = 2;
}
