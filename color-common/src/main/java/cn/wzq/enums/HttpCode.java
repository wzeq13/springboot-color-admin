package cn.wzq.enums;

/**
 * Http状态码
 *
 * @Author wzq
 * @Date 2021-03-07 23:32
 * @Version 1.0
 **/
public enum HttpCode {
    /** 成功状态 **/
    SUCCESS(200,"响应成功"),
    FAILED(400,"响应失败"),
    NOTPERMIT(401,"权限不足");

    /** 状态码 **/
    private int code;

    /** 返回消息 **/
    private String msg;

    HttpCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
