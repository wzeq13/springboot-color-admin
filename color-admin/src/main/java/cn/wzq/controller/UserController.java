package cn.wzq.controller;

import cn.wzq.domain.AjaxResult;
import cn.wzq.domain.BaseController;
import cn.wzq.domain.entity.User;
import cn.wzq.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wzq
 * @Date 2021-03-09 11:39
 * @Version 1.0
 **/
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {
    @Autowired
    private IUserService userService;

    @PostMapping
    @ApiOperation("添加用户")
    public AjaxResult addUser(@RequestBody User user){
        return toAjax(userService.addUser(user));
    }
}
