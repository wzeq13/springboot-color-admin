package cn.wzq.controller;

import cn.wzq.domain.AjaxResult;
import cn.wzq.web.service.CaptchaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author wzq
 * @Date 2021-03-15 15:31
 * @Version 1.0
 **/
@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private CaptchaService captchaService;

    @GetMapping("/generate/captcha")
    @ApiOperation("生成验证码")
    public void generateCaptcha(HttpServletRequest request, HttpServletResponse response){
        captchaService.generateCaptcha(request, response);
    }
}
