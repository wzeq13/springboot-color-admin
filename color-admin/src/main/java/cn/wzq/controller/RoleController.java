package cn.wzq.controller;

import cn.wzq.domain.AjaxResult;
import cn.wzq.domain.BaseController;
import cn.wzq.domain.entity.Role;
import cn.wzq.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wzq
 * @Date 2021-03-09 21:03
 * @Version 1.0
 **/
@RestController
@RequestMapping("/api/role")
public class RoleController extends BaseController {

    @Autowired
    private IRoleService roleService;

    @PostMapping
    @ApiOperation("添加角色")
    public AjaxResult addRole(@RequestBody Role role){
        return toAjax(roleService.addRole(role));
    }
}
