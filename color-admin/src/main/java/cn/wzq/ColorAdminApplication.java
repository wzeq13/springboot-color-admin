package cn.wzq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Author wzq
 * @Date 2021-03-07 23:18
 * @Version 1.0
 **/
@SpringBootApplication
public class ColorAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(ColorAdminApplication.class,args);
    }
}
