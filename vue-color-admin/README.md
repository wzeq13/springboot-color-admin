# 关于vue-color-admin
起初vue-color-admin并不是一个纯前端的后台模板，早期是使用java+模板引擎实现的，在近些年前后端分离大大趋势，我决定使用vue对原有的代码进行重构，由于自己并没有很系统研究过vue，目前可能会存在一些bug，我会尽自己努力去解决bug


# 简介
vue-color-admin是一个后台管理系统模板，其采用vue3+webpack+element-plus实现，它可以快速的搭建后台模板，可以大大节省后端人员搭建页面的时间

[在线测试地址](http://47.98.103.2:7001/)

**该项目目前只进行了一些简单的移动端适配，所以对小屏幕设备支持可能不太好，后续会持续改进**

# 快速使用
1. 克隆仓库
```bash
git clone https://gitee.com/wzeq13/vue-color-admin.git
```

2. 进入目录并且安装依赖
```bash
cd vue-color-admin
yarn or npm install
```

3. 运行项目
```bash
yarn serve or npm run serve
```

# 项目技术栈
感谢以下大佬提供的技术
- [vue-next](https://v3.vuejs.org/) 
- [element-plus](https://element-plus.gitee.io/#/zh-CN) 饿了么团队ui库
- [axios](http://www.axios-js.com/) 轻量级ajax库
- [countup.js](https://github.com/inorganik/countUp.js) 计数器库
- [highlight](https://highlightjs.org/) 代码着色工具
- [markdown-it](https://github.com/markdown-it/markdown-it)   markdown转化工具
- [mockjs](http://mockjs.com/) 生成随机数据，拦截 Ajax 请求
- [normalize.css](https://github.com/necolas/normalize.css) 统一浏览器样式
- [screenfull](https://github.com/sindresorhus/screenfull.js) javascript全屏api
- [sortablejs](https://github.com/SortableJS/ngx-sortablejs)
- [vuedraggable-next](https://github.com/SortableJS/vue.draggable.next) vue拖拽控件
- [tinymce](https://www.tiny.cloud/) 富文本编辑器

# 平台功能
- 自动生成面包屑导航
- 根据路由自动生成菜单栏
- 计数器组件
- 音乐播放器
- 两种表单
- 三组表格
- 富文本编辑器
- markdown编辑器
- 拖拽面板
- 拖拽列表
- 图表

# License
MIT