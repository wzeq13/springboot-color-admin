import request from '../utils/request'

export function queryStudent(start,size){
    return request({
        url: '/student',
        method: 'GET',
        params:{
            start: start,
            size: size
        }
    })
}