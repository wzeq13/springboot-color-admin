import Mock from 'mockjs'
import studentList from './student'

Mock.setup({
    timeout: '0-900'
})

Mock.mock(/\/student/,'get',(data)=>{
    let start = getParam(data.url,'start')
    let size = getParam(data.url,'size')
    if(start == ''){
        start = 1
    }
    if(size == ''){
        size = 10
    }
    start = Number(start)
    size = Number(size)
    return setAjax(200,"查询成功",{
        start: start,
        size: size,
        total: studentList.length,
        list: dealPage(start,size,studentList)
    })
})


const getParam = (url,name)=>{
    const params = url.split('?')[1].split('&')
    const param = params.filter(val=>{
        return val.match(name)
    })
    if(param.length == 0){
        return ''
    }
    return param[0].split('=')[1]
}


const dealPage = (start,size,list) => {
    const listLength = list.length
    const startPos = (listLength / size) * (start-1)
    
    if( (startPos + size) > (listLength-1)){
        size = listLength-startPos-1
    }
    console.log(startPos,size);
    const listTemp = [...list]
    return listTemp.splice(startPos,size)
}

const setAjax = (code,msg,data)=>{
    return {
        code: code,
        msg: msg,
        data: data
    }
}