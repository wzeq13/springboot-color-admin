import Mock from 'mockjs'

const {Random} = Mock

let data = []

for(let i = 0;i<100;i++){
    let template = {
        id: Random.id(),
        studentnumber: Random.natural(10000000000,99999999999),
        name: Random.cname(),
        gender:Random.pick(['男','女']),
        score: Random.float(0,100,0,2),
        state: Random.pick(['通过','未通过','待审核']),
        date: Random.date(),
        address: Random.county(true)
    }
    data.push(template)
}

export default data