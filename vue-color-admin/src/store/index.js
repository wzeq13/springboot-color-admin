import { createStore } from 'vuex'
import menu from './moudles/menu'
import tab from './moudles/tab'
import setting from './moudles/setting'

export default createStore({
  state:{},
  mutations:{},
  actions:{},
  modules: {
    menu,tab,setting
  }
})
