const state = {
    isLogo: true,
    isTag: true
};
const mutations = {
    TOGGLELOGO(state){
        state.isLogo = ! state.isLogo
    },
    TOGGLETAG(state){
        state.isTag = ! state.isTag
    }
};
const actions = {
    toggleLogo(context){
        context.commit('TOGGLELOGO')
    },
    toggleTag(context){
        context.commit('TOGGLETAG')
    }
};
const getters = {

};
   
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}