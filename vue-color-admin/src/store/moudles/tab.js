const state = {
    index: 0,
    tabList: [
        {
            icon: "yibiaopan",
            content: '仪表盘',
            path: '/dashboard',
            isOpen: true
        }
    ]
};
const mutations = {
    ADD_TAB: (state,tabItem)=>{
        const res = state.tabList.findIndex(val=> val.path == tabItem.path)
        if(res === -1){
            state.tabList.push(tabItem)
            state.index = state.tabList.length - 1
        }else{
            state.index = res
        }
    },
    DELETE_TAB: (state,path)=>{
        const res = state.tabList.findIndex(val=> val.path == path)
        if(res != -1){
            state.tabList.splice(res,1)
            state.index = res-1
            if(state.index < 0){
                state.tabList.push({
                    icon: "yibiaopan",
                    content: '仪表盘',
                    path: '/dashboard',
                    isOpen: true
                })

                state.index = 0
            }
        }
    },
    CLICK_TAB: (state,path)=>{
        const res = state.tabList.findIndex(val=> val.path == path)
        state.index = res
    }
    
};
const actions = {
    addTab(context,tabItem){
        context.commit('ADD_TAB',tabItem)
    },
    closeTab(context,path){
        context.commit('DELETE_TAB',path)
    },
    clickTab(context,path){
        context.commit('CLICK_TAB',path)
    }
};
const getters = {

};
   
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}