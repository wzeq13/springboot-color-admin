const state = {
    isCollaspe: false
};
const mutations = {
    COLLAPSE(state){
        state.isCollaspe = ! state.isCollaspe
    }
};
const actions = {
    collapse(context){
        context.commit('COLLAPSE')
    }
};
const getters = {

};
   
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}