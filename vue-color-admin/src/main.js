import { createApp,devtools } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'normalize.css'
import './scss/main.scss'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import SvgIcon from './components/SvgIcon'
import Introduce from './components/introduce'
import './mock/index'

const app = createApp(App);
app.use(store).use(router).use(ElementPlus).mount('#app')
app.component('SvgIcon',SvgIcon)
app.component('Introduce',Introduce)
