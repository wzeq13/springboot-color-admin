import { createRouter, createWebHistory } from 'vue-router'
import MainLayout from '../layouts/MainLayout.vue'
import Login from '../views/Login'
import DashBoard from '../views/DashBoard'


const routes = [
    {
      path: '/login',
      component: Login,
      noShow: true
    },  
    {
      path: '/',
      component: MainLayout,
      redirect: '/dashboard',
      children:[
        {
          path: 'dashboard',
          component: DashBoard,
          meta:{
            title: "仪表盘",
            icon: 'yibiaopan',
            affix: true
          }
        }
      ]
    },
    {
      path: '/form',
      redirect: '/form/commonform',
      component: MainLayout,
      meta:{
        title: '表单',
        icon: 'biaodan'
      },
      children:[
        {
          path: 'commonform',
          component: ()=>import('../views/Form/CommonForm'),
          meta: {
            title: "普通表单",
            icon: 'biaodan'
          }
        },
        {
          path: 'stepform',
          component: ()=>import('../views/Form/StepForm'),
          meta: {
            title: "分段表单",
            icon: 'biaodan'
          }
        }
      ]
    },
    {
      path: '/table',
      redirect: '/table/commontable',
      component: MainLayout,
      meta:{
        title: '表格',
        icon: 'table'
      },
      children:[
        {
          path: 'commontable',
          component: ()=>import('../views/Table/CommonTable'),
          meta: {
            title: "普通表格",
            icon: 'table'
          }
        },
        {
          path: 'dragtable',
          component: ()=>import('../views/Table/DragTable'),
          meta: {
            title: "拖拽表格",
            icon: 'table'
          }
        },
        {
          path: 'dynatable',
          component: ()=>import('../views/Table/DynamicTable'),
          meta: {
            title: "动态表格",
            icon: 'table'
          }
        }
      ]
    },
    {
      path: '/edit',
      redirect: '/edit/markdown',
      component: MainLayout,
      meta: {
        title: '编辑器',
        icon: 'edit'
      },
      children:[
        {
          path: 'markdown',
          component: ()=>import('../views/Editor/MarkDown'),
          meta: {
            title: "MarkDown",
            icon: 'edit'
          }
        },
        {
          path: 'richtext',
          component: ()=>import('../views/Editor/RichText'),
          meta: {
            title: "富文本编辑器",
            icon: 'edit'
          }
        }
      ]
    },
    {
      path: '/chart',
      redirect: '/chart/linechart',
      component: MainLayout,
      meta: {
        title: '图表',
        icon: 'chart'
      },
      children:[
        {
          path: 'linechart',
          component: ()=>import('../views/Chart/LineChat'),
          meta: {
            title: "折线图",
            icon: 'chart'
          }
        },
        {
          path: 'barchart',
          component: ()=>import('../views/Chart/BarChat'),
          meta: {
            title: "柱状图",
            icon: 'chart'
          }
        },
        {
          path: 'doughnut',
          component: ()=>import('../views/Chart/DoughunuChart'),
          meta: {
            title: "环形图",
            icon: 'chart'
          }
        }
      ]
    },
    {
      path: '/drag',
      redirect: '/drag/pannel',
      component: MainLayout,
      meta:{
        title: '拖拽组件',
        icon: 'move'
      },
      children:[
        {
          path: 'pannel',
          component: ()=>import('../views/DragComponents/DragPannel'),
          meta:{
            title: "拖拽看板",
            icon: 'move'
          }
        },
        {
          path: 'list',
          component: ()=>import('../views/DragComponents/DragList'),
          meta:{
            title: "拖拽列表",
            icon: 'move'
          }
        }
      ]
    },
    {
      path: '/music',
      redirect: '/music/index',
      component: MainLayout,
      meta: {
        title: '音乐播放器',
        icon: 'music'
      },
      children:[
        {
          path: 'index',
          component: ()=>import('../views/Music'),
          meta: {
            title: "音乐播放器",
            icon: 'music'
          }
        }
      ]
    },
    {
      path: '/countup',
      redirect: '/countup/index',
      component: MainLayout,
      meta: {
        title: '计数组件',
        icon: 'music'
      },
      children:[
        {
          path: 'index',
          component: ()=>import('../views/CountUp'),
          meta: {
            title: "计数组件",
            icon: 'jishuqi'
          }
        }
      ]
    }
    
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
